﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using colinlboyledotcom.Models;
using colinlboyledotcom.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace colinlboyledotcom.Controllers
{
    public class GalleryController : Controller
    {
        private readonly GalleryRepository galleryRepository;

        public GalleryController(IConfiguration configuration)
        {
            galleryRepository = new GalleryRepository(configuration);
        }

        // GET: Images
        public ActionResult All()
        {
            return View(galleryRepository.FindAll());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Create(Image image)
        {
            if (!ModelState.IsValid) return View(image);
            galleryRepository.Add(image);
            return RedirectToAction("All");
        }
    }
}
