﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using colinlboyledotcom.Models;
using colinlboyledotcom.Repository;
using Microsoft.AspNetCore.Authorization;

namespace colinlboyledotcom.Controllers
{
    public class HomeController : Controller
    {
        private readonly BlogPostRepository blogPostRepository;

        public HomeController(IConfiguration configuration)
        {
            blogPostRepository = new BlogPostRepository(configuration);
        }

        public IActionResult Index()
        {
            Posts models = new Posts();
            models.BlogPosts = blogPostRepository.FindTop(0);
            //Find tags (((This is calling blogtag from blogpostrepository probably not optimal)))//
            foreach (BlogPost post in models.BlogPosts)
            {
                post.BlogTags = new List<BlogTag>();
                post.BlogTags = blogPostRepository.FindTags(post.bpId);
            }
            models.FeaturedPosts = blogPostRepository.FindFeatured();
            foreach (BlogPost post in models.FeaturedPosts)
            {
                post.BlogTags = new List<BlogTag>();
                post.BlogTags = blogPostRepository.FindTags(post.bpId);
            }
            return View(models);
        }

        public IActionResult About()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendContact(Contact contact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(contact.Email);
                    mailMessage.To.Add("colin.boyle93@gmail.com");
                    mailMessage.Subject = contact.Subject;
                    mailMessage.Body = $"New mesage from clb contact page. Email: {contact.Email} Message: {contact.Message}";

                    SmtpClient smtp = new SmtpClient("smtp.gmail.com");
                    smtp.Credentials = new System.Net.NetworkCredential("automatedclb@gmail.com", "2@reccoS2");
                    smtp.EnableSsl = true;
                    smtp.Port = 587;

                    smtp.Send(mailMessage);

                    ModelState.Clear();
                    ViewBag.Message = "Thank you for reaching out to me!";
                }
                catch (Exception err)
                {
                    ModelState.Clear();
                    ViewBag.Message = $" Yikes! There was an issue with your message. {err.Message}";
                }
            }
            return View("Contact");
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
