﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using colinlboyledotcom.Models;
using colinlboyledotcom.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace colinlboyledotcom.Controllers
{
    public class NewsletterController : Controller
    {
        private readonly SubscribeRepository subRepository;

        public NewsletterController(IConfiguration configuration)
        {
            subRepository = new SubscribeRepository(configuration);
        }

        //POST: subscribe
        [HttpPost]
        public ActionResult Subscribe(string email)
        {
            Subscriber subscribe = new Subscriber();
            subscribe.email = email;
            if (User.Identity.Name != null)
            {
                subscribe.username = User.Identity.Name;
            }
            subscribe.subscribeddate = DateTime.Now;
            subRepository.Add(subscribe);
            return RedirectToAction("index", "home");
        }
    }
}