﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using colinlboyledotcom.Models;
using colinlboyledotcom.Repository;
using Microsoft.AspNetCore.Authorization;

namespace colinlboyledotcom.Controllers
{
    public class BlogController : Controller
    {
        private readonly BlogPostRepository blogPostRepository;
        private readonly PostTagRepository postTagRepository;
        private readonly BlogTagRepository blogTagRepository;
        private readonly BlogCommentRepository blogCommentRepository;

        public BlogController(IConfiguration configuration)
        {
            blogPostRepository = new BlogPostRepository(configuration);
            postTagRepository = new PostTagRepository(configuration);
            blogTagRepository = new BlogTagRepository(configuration);
            blogCommentRepository = new BlogCommentRepository(configuration);
        }

        // GET: Blog/All/1
        public ActionResult All(int? page)
        {
            Posts model = new Posts();
            if (page.HasValue)
            {
                int pagenum = page.Value;
                if (pagenum <= 0)
                {
                    return RedirectToAction("index", "home");
                }
                else
                {
                    model.BlogPosts = blogPostRepository.FindTop(pagenum);
                    foreach( BlogPost post in model.BlogPosts)
                    {
                        List<BlogTag> Tags = new List<BlogTag>();
                        post.BlogTags = blogPostRepository.FindTags(post.bpId);
                    }
                    model.PopularTags = blogTagRepository.FindPopular();
                    return View(model);
                }

            }
            return View();
        }

        // GET: Blog/Search/searchString
        public ActionResult Search(string searchString, int? page)
        {
            if (!string.IsNullOrEmpty(searchString))
            {
                if (page == null || page <= 0) { page = 1; }
                int pagenum = page.Value - 1;
                Posts model = new Posts();
                model.BlogPosts = blogPostRepository.Search(searchString, pagenum);
                foreach (BlogPost post in model.BlogPosts)
                {
                    List<BlogTag> Tags = new List<BlogTag>();
                    post.BlogTags = blogPostRepository.FindTags(post.bpId);
                }
                model.type = "search";
                model.parameter = searchString;
                model.PopularTags = blogTagRepository.FindPopular();
                return View("All", model);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }

        // GET: Blog/Article/5
        public ActionResult Article(int id)
        {
            //Selecting the article/
            BlogPost blogPost = blogPostRepository.FindByID(id);

            //Selecting the previous and next article and saving them to the post.PrevPosts and post.NextPost//
            //BlogPost PrevPost = 
            //BlogPost NextPost = 

            //Selcting comments of the article//
            IEnumerable<BlogComment> comments = blogCommentRepository.FindByPost(id);

            //The tags returned for the specific article//
            IEnumerable<BlogTag> tags = blogPostRepository.FindTags(id);

            //The Post model holds the BlogPost, its tags, its comments, and the previous and next article//
            Post post = new Post();
            post.BlogPost = blogPost;
            post.BlogTags = tags;
            post.PrevPost = blogPostRepository.ClosePost(id, "DESC");
            post.NextPost = blogPostRepository.ClosePost(id, "ASC");
            post.PopularTags = blogTagRepository.FindPopular();
            post.BlogComments = comments;
            if (blogPost == null)
            {
                return NotFound();
            }
            return View(post);
        }

        // POST: blog/comment (comment)
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Comment(BlogComment blogComment)
        {
            if (ModelState.IsValid)
            {
                blogComment.CreatedDate = DateTime.Now;
                BlogPost post = blogPostRepository.FindByID(blogComment.PostId);
                post.Comments++;
                blogPostRepository.Update(post);
                blogCommentRepository.Add(blogComment);
                return RedirectToAction("article", "blog", new { id = blogComment.PostId });
            }
            return RedirectToAction("index", "home");
        }
        
        // GET: blog/tag/tech
        public ActionResult Tag(string tag, int? page)
        {
            Posts model = new Posts();
            if (string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("index", "home");
            }
            else if (page == null || page <= 0)
            {
                page = 1;
            }
            int pagenum = page.Value - 1;
            model.BlogPosts = blogPostRepository.FindByTag(tag, pagenum);
            foreach (BlogPost post in model.BlogPosts)
            {
                List<BlogTag> Tags = new List<BlogTag>();
                post.BlogTags = blogPostRepository.FindTags(post.bpId);
            }
            model.PopularTags = blogTagRepository.FindPopular();
            model.type = "tag";
            model.parameter = tag;

            return View("all", model);
        }
        //
        // GET: Blog/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }
        
        // POST: Blog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create( BlogPost blogPost, BlogTag blogTag)
        {
            if (ModelState.IsValid)
            {
                blogPost.CreatedDate = DateTime.Now;
                blogPostRepository.Add(blogPost);
                BlogPost newPost = blogPostRepository.FindByTitle(blogPost.Title);
                IEnumerable<string> blogTags = blogTag.Tag_Name.Split(',').ToList();
                foreach (string tag in blogTags)
                {
                    int tagint = blogTagRepository.FindByName(tag);
                    if (tagint == 0)
                    {
                        BlogTag newTag = new BlogTag();
                        newTag.Tag_Name = tag;
                        newTag.Tag_Frequency = 1;
                        blogTagRepository.Add(newTag);
                    }
                    else
                    {
                        BlogTag oldTag = blogTagRepository.FindByID(tagint);
                        oldTag.Tag_Frequency++;
                        blogTagRepository.Update(oldTag);
                    }
                    PostTag postTag = new PostTag { PostId = newPost.bpId, TagId = blogTagRepository.FindByName(tag) };
                    postTagRepository.Add(postTag);
                }
                return RedirectToAction("all");
            }
        
            return View(blogPost);
        }
        
        // GET: Blog/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new BadRequestResult();
            }
            int bpid = id.Value;
            BlogPost blogPost = blogPostRepository.FindByID(bpid);
            if (blogPost == null)
            {
                return View("All");
            }
            return View(blogPost);
        }
        
        // POST: Blog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( BlogPost blogPost)
        {
            if (ModelState.IsValid)
            {
                blogPostRepository.Update(blogPost);
                return RedirectToAction("all");
            }
            return View(blogPost);
        }
        
        // GET: Blog/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new BadRequestResult();
            }
            int bpid = id.Value;
            BlogPost blogPost = blogPostRepository.FindByID(bpid);
            if (blogPost == null)
            {
                return View("all");
            }
            return View(blogPost);
        }
        
        // POST: Blog/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            blogPostRepository.Remove(id);
            return RedirectToAction("all");
        }
        
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}