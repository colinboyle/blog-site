﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using colinlboyledotcom.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using colinlboyledotcom.Data;
using colinlboyledotcom.Services;
using Hangfire;
using Hangfire.PostgreSql;
using System.Diagnostics;

namespace colinlboyledotcom
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddEntityFrameworkNpgsql().AddDbContext<ApplicationDbContext>(opt =>
                opt.UseNpgsql(Configuration.GetConnectionString("Blog")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc();

            /*services.AddHangfire(config =>config.UsePostgreSqlStorage(Configuration.GetConnectionString("Blog")));*/


            var connectionStringBlog = Configuration.GetConnectionString("Blog");
            services.AddEntityFrameworkNpgsql().AddDbContext<BlogDataContext>(name => name.UseNpgsql(connectionStringBlog));
            services.AddEntityFrameworkNpgsql().AddDbContext<GalleryDataContext>(name => name.UseNpgsql(connectionStringBlog));

            services.AddSingleton<IConfiguration>(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            /*app.UseHangfireServer();
            app.UseHangfireDashboard("/dashboard");
            RecurringJob.AddOrUpdate(
                () => Debug.WriteLine("Minutely Job"), Cron.Weekly);*/

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto
            });

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "Tags",
                    template: "blog/tag/{tag}/{page}",
                    defaults: new { controller = "blog", action = "tag", page = 1 }
                );

                routes.MapRoute(
                    name: "Search",
                    template: "blog/search/{page}/{searchString}",
                    defaults: new { controller = "blog", action = "search", page = 1 }
                );

                routes.MapRoute(
                    name: "Blog",
                    template: "blog/{action}/{page}",
                    defaults: new { controller = "blog", action = "all", page = 1 }
                );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
