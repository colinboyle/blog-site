﻿
using Microsoft.EntityFrameworkCore;

namespace colinlboyledotcom.Models
{
    public class BlogDataContext : DbContext
    {
        public BlogDataContext(DbContextOptions<GalleryDataContext> name) : base(name)
        {

        }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<BlogTag> BlogTags { get; set; }
        public DbSet<PostTag> PostTags { get; set; }
        public DbSet<BlogComment> BlogComments { get; set; }
    }
}
