﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace colinlboyledotcom.Models
{
    public class Subscriber : BaseEnitity
    {
        public int sid { get; set; }

        [StringLength(256)]
        [EmailAddress]
        public string email { get; set; }

        [StringLength(256)]
        public string username { get; set; }

        public DateTime subscribeddate { get; set; }
    }
}
