﻿namespace colinlboyledotcom.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PostTag")]
    public partial class PostTag : BaseEnitity
    {
        [Key]
        public int ptId { get; set; }

        public int PostId { get; set; }

        public int TagId { get; set; }
    }
}
