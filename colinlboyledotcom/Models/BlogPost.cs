﻿namespace colinlboyledotcom.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BlogPost : BaseEnitity
    {
        [Key]
        public int bpId { get; set; }

        public string Title { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime CreatedDate { get; set; }

        public string Image { get; set; }

        public string Summary { get; set; }

        public string Post { get; set; }

        public int Views { get; set; }

        public int Comments { get; set; }

        public int Likes { get; set; }

        public bool Status { get; set; }

        public bool Featured { get; set; }

        public List<BlogTag> BlogTags { get; set; }
    }
}
