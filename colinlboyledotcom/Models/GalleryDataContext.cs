﻿using Microsoft.EntityFrameworkCore;

namespace colinlboyledotcom.Models
{
    public class GalleryDataContext : DbContext
    {
        public GalleryDataContext(DbContextOptions<GalleryDataContext> name) : base(name)
        {

        }
        public DbSet<Image> Images { get; set; }
    }
}