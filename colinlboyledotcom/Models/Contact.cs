﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace colinlboyledotcom.Models
{
    public class Contact
    {
        [Required]
        [StringLength(25,ErrorMessage = "I was only expecting between 5 and 25 characters",MinimumLength = 5)]
        public string Name { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Hmm.. Do you have a different email address?")]
        public string Email { get; set; }

        [Required(ErrorMessage = "What is this message is about?")]
        public string Subject { get; set; }
       
        [Required(ErrorMessage = "You'll have to say more than that.")]
        public string Message { get; set; }
    }
}
