﻿namespace colinlboyledotcom.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogTag")]
    public partial class BlogTag : BaseEnitity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int btId { get; set; }

        [Required]
        [StringLength(20)]
        public string Tag_Name { get; set; }

        public int Tag_Frequency { get; set; }
    }
}
