﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace colinlboyledotcom.Models
{
    public class Posts : BaseEnitity
    {
        public IEnumerable<BlogPost> BlogPosts { get; set; }
        public IEnumerable<BlogPost> FeaturedPosts { get; set; }
        public List<BlogTag> PopularTags { get; set; }
        public string type { get; set; }
        public string parameter { get; set; }
    }
}
