﻿namespace colinlboyledotcom.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    
    public partial class BlogComment : BaseEnitity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int bcId { get; set; }

        [Required]
        [StringLength(256)]
        public string UserId { get; set; }

        public int PostId { get; set; }

        [Column(TypeName = "datetime2")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(240)]
        public string Comment { get; set; }

        public bool Status { get; set; }

        public int? ReplyId { get; set; }
    }
}