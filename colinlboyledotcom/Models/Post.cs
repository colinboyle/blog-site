﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace colinlboyledotcom.Models
{
    public class Post
    {
        public BlogPost BlogPost { get; set; }
        public BlogPost PrevPost { get; set; }
        public BlogPost NextPost { get; set; }
        //Adding tag model?//
        public BlogTag BlogTag { get; set; }
        public IEnumerable<BlogTag> BlogTags { get; set; }
        //Adding comment model//
        public BlogComment BlogComment { get; set; }
        public IEnumerable<BlogComment> BlogComments { get; set; }
        //Popular tag sidebar//
        public List<BlogTag> PopularTags { get; set; }
    }
}
