﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Npgsql;
using System.Threading.Tasks;
using colinlboyledotcom.Models;

namespace colinlboyledotcom.Repository
{
    public class BlogPostRepository : IRepository<BlogPost>
    {
        private string connectionString;

        private readonly PostTagRepository postTagRepository;
        private readonly BlogTagRepository blogTagRepository;
        private readonly BlogCommentRepository blogCommentRepository;

        public BlogPostRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("ConnectionStrings:Blog");
            postTagRepository = new PostTagRepository(configuration);
            blogTagRepository = new BlogTagRepository(configuration);
            blogCommentRepository = new BlogCommentRepository(configuration);
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(BlogPost item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("INSERT INTO blogpost (title,createddate,image,summary,post,status,featured) VALUES(@Title,@CreatedDate,@Image,@Summary,@Post,@Status,@Featured)", item);
            }

        }

        public IEnumerable<BlogPost> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogPost>("SELECT * FROM blogpost WHERE status = true");
            }
        }

        //Returns the last five posts and skips by five(int page) used by index.home and all.blog//
        public IEnumerable<BlogPost> FindTop(int page)
        {
            int offset = page * 5;
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogPost>("SELECT * FROM blogpost WHERE status = true ORDER BY createddate DESC LIMIT 5 OFFSET " + offset);
            }
        }

        //Returns the last five posts and skips by five(int page) used by index.home and all.blog//
        public IEnumerable<BlogPost> FindFeatured()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogPost>("SELECT * FROM blogpost WHERE status = true AND featured = true ORDER BY createddate DESC LIMIT 3");
            }
        }

        //Find by the tag name for blog/tag route//
        public List<BlogPost> FindByTag(string tag, int page)
        {
            int offset = page * 5;
            List<BlogPost> posts = new List<BlogPost>();
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                int tagid = blogTagRepository.FindByName(tag);
                if (tagid == 0) { return (posts); }
                IEnumerable<int> postids = postTagRepository.FindTagID(tagid);
                var ids = string.Join(",", postids);
                Console.WriteLine(ids);
                posts = dbConnection.Query<BlogPost>("SELECT * FROM blogpost WHERE status = true AND bpid IN (" + ids + ") ORDER BY createddate DESC LIMIT 5 OFFSET " + offset).ToList();
                return (posts);
            }
        }

        //Search blogs titles by the search query field// (Make this better eventually!)
        public IEnumerable<BlogPost> Search(string query, int page)
        {
            int offset = page * 5;
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogPost>("SELECT * FROM blogpost WHERE status = true AND to_tsvector('english', title) @@ to_tsquery('english', '" + query + "') ORDER BY createddate DESC LIMIT 5 OFFSET " + offset);
            }
        }

        //Finds comments from blogcomment db by postid//
        public IEnumerable<BlogComment> FindComments(int postid)
        {
            IEnumerable<BlogComment> comments = new List<BlogComment>();
            comments = blogCommentRepository.FindByPost(postid);
            return comments;
        }

        //Links tags to post from posttag and blogtag db//
        public List<BlogTag> FindTags(int bpid)
        {
            List<BlogTag> Tags = new List<BlogTag>();
            IEnumerable<int> postTags = postTagRepository.FindPostID(bpid);
            foreach (int postTag in postTags)
            {
                BlogTag blogTag = blogTagRepository.FindByID(postTag);
                Tags.Add(blogTag);
            }
            return Tags;
        }

        //Find Previous and Next post//
        public BlogPost ClosePost(int currentid, string ascdesc)
        {
            string sign = ">";
            if (ascdesc == "DESC") { sign = "<"; }
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogPost>("SELECT * FROM blogpost WHERE status = true AND title IS NOT NULL AND bpid " + sign + " " + currentid + " ORDER BY bpid " + ascdesc + " LIMIT 1").FirstOrDefault();
            }
        }

        //Find post with postid from posttag db//
        public BlogPost FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogPost>("SELECT * FROM blogpost WHERE status = true AND bpid = @bpid", new { bpid = id }).FirstOrDefault();
            }
        }

        //Find post with postid from posttag db//
        public BlogPost FindByTitle(string title)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogPost>("SELECT * FROM blogpost WHERE status = true AND title = @title", new { title = title }).FirstOrDefault();
            }
        }

                public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM blogpost WHERE bpid=@bpid", new { bpid = id });
            }
        }

        public void Update(BlogPost item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("UPDATE blogpost SET title = @Title, createddate = @CreatedDate, image = @Image, summary = @Summary, post = @Post, status = @Status, featured = @Featured, comments = @Comments WHERE bpid = @bpid", item);
            }
        }
    }
}
