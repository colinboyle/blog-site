﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Npgsql;
using System.Threading.Tasks;
using colinlboyledotcom.Models;

namespace colinlboyledotcom.Repository
{
    public class BlogTagRepository : IRepository<BlogTag>
    {
        private string connectionString;
        public BlogTagRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("ConnectionStrings:Blog");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(BlogTag item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("INSERT INTO blogtag (tag_name, tag_frequency) VALUES(@Tag_Name,@Tag_Frequency)", item);
            }

        }

        public IEnumerable<BlogTag> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogTag>("SELECT * FROM blogtag");
            }
        }

        public BlogTag FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogTag>("SELECT * FROM blogtag WHERE btid = @btid", new { btid = id }).FirstOrDefault();
            }
        }

        public int FindByName(string tag)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<int>("SELECT btid FROM blogtag WHERE tag_name = @tag_name", new { tag_name = tag }).FirstOrDefault();
            }

        }

        public List<BlogTag> FindPopular()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogTag>("SELECT * FROM blogtag ORDER BY tag_frequency DESC LIMIT 6").ToList();
            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM blogtag WHERE btid=@Id", new { btid = id });
            }
        }

        public void Update(BlogTag item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("UPDATE blogtag SET tag_frequency  = @Tag_Frequency WHERE btid = @btid", item);
            }
        }
    }
}
