﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Npgsql;
using System.Threading.Tasks;
using colinlboyledotcom.Models;

namespace colinlboyledotcom.Repository
{
    public class SubscribeRepository : IRepository<Subscriber>
    {
        private string connectionString;
        public SubscribeRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("ConnectionStrings:Blog");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Subscriber item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("INSERT INTO subscribers (email, username, subscribeddate) VALUES(@Email,@UserName, @SubscribedDate)", item);
            }

        }

        public IEnumerable<Subscriber> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<Subscriber>("SELECT * FROM subscribers");
            }
        }

        public Subscriber FindByID(int sid)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<Subscriber>("SELECT * FROM subscribers WHERE sid = @sid", new { sid = sid }).FirstOrDefault();
            }
        }

        public void Remove(int sid)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM subscribers WHERE sid=@sid", new { sid = sid });
            }
        }

        public void Update(Subscriber item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("UPDATE subscribers SET email  = @Email, username = @UserName WHERE sid = @sid", item);
            }
        }
    }
}
