﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Npgsql;
using System.Threading.Tasks;
using colinlboyledotcom.Models;

namespace colinlboyledotcom.Repository
{
    public class PostTagRepository : IRepository<PostTag>
    {
        private string connectionString;
        public PostTagRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("ConnectionStrings:Blog");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(PostTag item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("INSERT INTO posttag (postid, tagid) VALUES(@postId,@tagID)", item);
            }

        }

        public IEnumerable<PostTag> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<PostTag>("SELECT * FROM posttag");
            }
        }

        public PostTag FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<PostTag>("SELECT * FROM posttag WHERE ptid = @Id", new { ptid = id }).FirstOrDefault();
            }
        }

        public IEnumerable<int> FindPostID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<int>("SELECT tagid FROM posttag WHERE postid = @postid", new { postid = id });
            }
        }

        public IEnumerable<int> FindTagID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<int>("SELECT postid FROM posttag WHERE tagid = @tagid", new { tagid = id });
            }
        }

        public void Remove(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM posttag WHERE ptid=@Id", new { ptid = id });
            }
        }

        public void Update(PostTag item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("UPDATE posttag SET postId = @postId,  tagid  = @tagId WHERE ptid = @Id", item);
            }
        }
    }
}
