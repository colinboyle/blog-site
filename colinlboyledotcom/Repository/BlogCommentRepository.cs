﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Dapper;
using colinlboyledotcom.Models;

namespace colinlboyledotcom.Repository
{
    public class BlogCommentRepository : IRepository<BlogComment>
    {
        private string connectionString;
        public BlogCommentRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("ConnectionStrings:Blog");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(BlogComment item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("INSERT INTO blogcomment (postid, userid, comment, createddate) VALUES(@PostId,@UserId,@Comment,@CreatedDate)", item);
            }

        }

        public IEnumerable<BlogComment> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogComment>("SELECT * FROM blogcomment");
            }
        }

        public IEnumerable<BlogComment> FindByPost(int postid)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogComment>("SELECT * FROM blogcomment WHERE status = true AND postid = " + postid);
            }
        }

        public BlogComment FindByID(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogComment>("SELECT * FROM blogcomment WHERE ptid = @Id", new { ptid = id }).FirstOrDefault();
            }
        }

        public IEnumerable<BlogComment> FindUserID(int userid)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<BlogComment>("SELECT * FROM blogcomment WHERE AND userid = @userid", new { userid = userid });
            }
        }

        public void Remove(int bcid)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM blogcomment WHERE bcid=@bcid", new { bcid = bcid });
            }
        }

        public void Update(BlogComment item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("UPDATE blogcomment SET postid = @postid,  userid  = @userid, comment = @comment, status = @status, createddate = @createddate WHERE bcid = @bcid", item);
            }
        }
    }
}
