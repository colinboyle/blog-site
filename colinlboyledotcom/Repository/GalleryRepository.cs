﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using colinlboyledotcom.Models;
using Npgsql;
using Dapper;
using System.Data;

namespace colinlboyledotcom.Repository
{
    public class GalleryRepository : IRepository<Image>
    {
        private string connectionString;
        public GalleryRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("ConnectionStrings:Blog");
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(Image item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("INSERT INTO images (title, description, imagepath) VALUES(@ImagePath,@Title,@Description)", item);
            }

        }

        public IEnumerable<Image> FindAll()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<Image>("SELECT * FROM images");
            }
        }

        public Image FindByID(int iid)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<Image>("SELECT * FROM images WHERE iid = @iid", new { iid = iid }).FirstOrDefault();
            }
        }

        public void Remove(int iid)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM images WHERE iid=@iid", new { iid = iid });
            }
        }

        public void Update(Image item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("UPDATE images SET title  = @Title, description = @Description, imagepath = @ImagePath WHERE iid = @iid", item);
            }
        }
    }
}
