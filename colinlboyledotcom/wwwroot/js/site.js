﻿(function ($) {
    "use strict";

    $.fn.andSelf = function () {
        return this.addBack.apply(this, arguments);
    }

    /*Show Menu*/
    $('.menu-trigger').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('#header').removeClass('menu-active')
        } else {
            $(this).addClass('active');
            $('#header').addClass('menu-active')
        }
    });

    /*Header Sticky*/
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 100) {
            $("#header.homeheader").addClass("add-bg")
        } else {
            $("#header.homeheader").removeClass("add-bg")
        }
    });

    /*==========================================================================================*/
    /*==============================Function Declaration==============================*/
    /*Featured Post Carousel*/
    function featuredPostCarosel() {
        if ($('.featured-post-carousel').length) {
            $('.featured-post-carousel').owlCarousel({
                items: 1,
                margin: 0,
                loop: false,
                nav: true,
                navContainer: '.featured-post-carousel',
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
            })
        }
    }

    /*Gallery Post*/
    function galleryOfPost() {
        if ($('.gallery-of-post').length) {
            $('.gallery-of-post').each(function () {
                $('.gallery-of-post').owlCarousel({
                    items: 2,
                    margin: 0,
                    loop: true,
                    nav: true,
                    dots: true
                })
            })
        }
    }

    /*Instafeed Widget*/

     var userFeed = new Instafeed({
         get: 'user',
         userId: '191992705',
         resolution: 'standard_resolution',
         accessToken: '191992705.1677ed0.4effe419321448b882ac1013a2195382',
         limit: 3,
         template: '<div class="item"><a href="{{image}}" data-source="{{image}}" title="{{caption}}"><img src="{{image}}" alt="{{caption}}"></a></div>',
         target: "instafeed",
         after: function () {
             var owl = $('.owl-carousel');
             owl.owlCarousel({
                 items: 1,
                 margin: 0,
                 loop: true,
                 animateIn: 'fadeIn',
                 animateOut: 'fadeOut',
                 autoplay: true,
                 autoplayTimeout: 8000,
                 autoplaySpeed: 3000,
                 autoplayHoverPause: true
             });
         }
     });

     userFeed.run();

    function zoomGallery() {
        if ($('#instafeed').length) {
            $('#instafeed').magnificPopup({  
                delegate: '.owl-item:not(.cloned) a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: false,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: true,
                    titleSrc: function (item) {
                        return item.el.attr('title') + ' &middot; <a class="image-source-link" href="' + item.el.attr('data-source') + '" target="_blank">image source</a>'
                    }
                },
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function (element) {
                        return element.find('img')
                    }
                }

            })
        }
    }

    /*Twitterfeed Widget*/
    function tick() {
        $('#tweets ul li:first').animate({ 'opacity': 0 }, 2000, function () {
            $(this).appendTo($('#tweets ul')).css('opacity', 0);
            $('#tweets ul li:first').css({ 'opacity': 1, 'display': 'block' });
        });
    }
    setInterval(function () { tick() }, 9000);


    /*Post Masonry*/
    function postMasonry() {
        if ($('#post-masonry').length) {
            var $container = $('#post-masonry');

            $container.imagesLoaded(function () {
                $container.isotope({
                    itemSelector: '.post',
                    layoutMode: 'masonry',
                })
            })
        }
    }

    /*Post Share*/
    function postShare() {
        if ($('.post-share').length) {
            $('.post-share button').on('click', function () {
                if ($(this).parent().hasClass('active')) {
                    $(this).parent().removeClass('active')
                } else {
                    $(this).parent().addClass('active')
                }
            })
        }
    }

    function thumbSilder() {
        // The slider being synced must be initialized first
        if ($('.thumbCarousel').length) {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                prevText: '<i class="fa fa-caret-left"></i>',
                nextText: '<i class="fa fa-caret-right"></i>',
                slideshow: false,
                itemWidth: 98,
                itemMargin: 10,
                asNavFor: '#slider'
            });

            $('#slider').flexslider({
                animation: "slide",
                controlNav: true,
                directionNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel"
            })
        }
    }

    /*==========================================================================================*/
    /*Function Call*/
    featuredPostCarosel();
    galleryOfPost();
    postMasonry();
    zoomGallery();
    postShare();
    thumbSilder()


})(jQuery)
